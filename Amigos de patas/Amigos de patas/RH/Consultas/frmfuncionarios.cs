﻿using Amigos_de_patas.telas.cadastros.classes.cadastrar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.telas.Consultas
{
    public partial class frmfuncionarios : Form
    {
        public frmfuncionarios()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = txtnome.Text.Trim();
                FuncionarioBusiness business = new FuncionarioBusiness();
                List<FuncionarioDTO> consultar = business.Consultar(dto);

                dgvfuncionario.AutoGenerateColumns = false;
                dgvfuncionario.DataSource = consultar;
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Preencha todos os campos.");
            }
            catch (Exception)
            {

                MessageBox.Show("Oh Oh! Algo de errado!");
            }
        }
    }
}
