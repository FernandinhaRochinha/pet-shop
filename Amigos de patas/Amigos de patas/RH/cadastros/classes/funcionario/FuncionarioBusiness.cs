﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.telas.cadastros.classes.cadastrar
{
    public class FuncionarioBusiness
    {
        FuncionarioDataBase db = new FuncionarioDataBase();
        public int Salvar(FuncionarioDTO dto)
        {
            
            return db.Salvar(dto);


        }
        public List<FuncionarioDTO> Listar()
        {
            return db.Listar();
        }
        public List<FuncionarioDTO> Consultar(FuncionarioDTO dto)
        {
            return db.Consultar(dto);
        }

        public void Remover (int ID)
        {
            db.Remover(ID);

        }
        public void Alterar(FuncionarioDTO dto )
        {
            db.Alterar(dto);

        }
    }
}
