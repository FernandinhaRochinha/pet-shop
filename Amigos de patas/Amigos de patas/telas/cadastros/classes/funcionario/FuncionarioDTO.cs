﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.telas.cadastros.classes.cadastrar
{
    class FuncionarioDTO
    {
        public int ID { get; set; }

        public int Departamento { get; set; }

        public int Idade { get; set; }

        public string CPF { get; set; }

        public string RG { get; set; }

        public decimal Salario { get; set; }

        public string Senha { get; set; }
    }
}
