﻿using Amigos_de_patas.telas.cadastros.classes.cadastrar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.telas.cadastros
{
    public partial class frmFuncionario : Form
    {
        public frmFuncionario()
        {
            InitializeComponent();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = txtnome.Text.Trim();
                dto.Departamento = Convert.ToInt32(cbodepartamento.Text.Trim());
                dto.Idade = Convert.ToInt32(txtidade.Text.Trim());
                dto.RG = txtrg.Text.Trim();
                dto.CPF = txtcpf.Text.Trim();
                dto.Salario = Convert.ToDecimal(txtsalario.Text.Trim());
                dto.Senha = txtrg.Text.Trim();
                dto.CEP = txtcep.Text.Trim();




                FuncionarioBusiness businesse = new FuncionarioBusiness();
                businesse.Salvar(dto);

                MessageBox.Show("Funcionario Salvo com sucesso!");
                this.Hide();

               

            }

            catch (Exception)
            {

                MessageBox.Show("Tente preencher todos os campos");
            }
        }
    }
}
